class Review
  class Create < Trailblazer::Operation
    extend ::Trailblazer::Operation::Contract::DSL

    contract ::Review::Contract::ReviewForm

    step ::Trailblazer::Operation::Model(::Review, :new)
    step ::TrailblazerHelpers::Steps::AssignCurrentUser
    success :setup_model!

    step ::Trailblazer::Operation::Contract::Build()
    step ::Trailblazer::Operation::Contract::Validate()
    step ::Trailblazer::Operation::Contract::Persist()

    step TrailblazerHelpers::Steps::AddModelToActivity

    def setup_model!(_options, params:, model:, **)
      params[:attachment_file] && model.create_attachment(file: params[:attachment_file])
    end
  end
end
