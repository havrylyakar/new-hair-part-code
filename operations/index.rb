class Folder
  class Index < Trailblazer::Operation
    step :setup_model!

    success ::TrailblazerHelpers::Steps::JsonApiParamsSetup

    def setup_model!(options, current_user:, **)
      options['model'] = current_user.folders
    end
  end
end
