class BookingQuery
  FILTERS = [DATE = 'date'.freeze].freeze

  DATE_FORMAT = '%Y-%m-%d'.freeze

  attr_reader :relation, :filters

  def initialize(relation = Booking.all, options = {})
    @relation = relation
    @filters = options[:filters]
  end

  def apply_filters
    filters!
    relation
  end

  def last_fb_friend_booking(user)
    Booking.joins(:user)
           .where(users: { facebook_id: user.facebook_friends_ids })
           .order(created_at: :desc).first&.user_id
  end

  private

  def filters!
    filters&.keys&.each { |el| send("filter_#{el}", filters[el]) if correct_filter?(el) }
  end

  def filter_date(date)
    @relation = relation.where('start_date > ? AND start_date < ?',
                               parse_date(date).beginning_of_day,
                               parse_date(date).end_of_day)
  end

  def parse_date(date)
    Time.zone.strptime(date, DATE_FORMAT)
  end

  def correct_filter?(filter_name)
    FILTERS.include?(filter_name) && filters[filter_name].present?
  end
end
