class ServiceQuery
  attr_reader :relation, :options

  def initialize(relation = Service.all, options = {})
    @relation = relation
    @options = options
  end

  def user_services
    Service.joins(category: :group).where(user_id: options[:params][:id],
                                          groups: { id: relation.id }).order('categories.name asc')
  end

  def service_with_price_booking(booking_id)
    relation.joins(:prices).where('prices.booking_id = ?', booking_id)
  end

  def service_without_price_booking(booking_id)
    relation.where.not(id: service_with_price_booking(booking_id))
  end

  def amount_services_for_category(categories)
    relation.joins(:category)
            .where('services.category_id IN (?)', categories)
            .sum('services.price')
  end
end
