class Review
  module Contract
    class ReviewForm < Reform::Form
      include Message::Contract::AttachmentFormModule

      properties(*Review.attribute_names)

      validates :rate, format: { with: FLOAT_NUMBER_REGEX_WITHOUT_0, multiline: true },
                       if: -> { rate.present? }
      validates :user_id, :rate, :booking_id, :description, presence: true

      validate :correct_reviewer?, if: -> { booking && user_id.present? }

      validate :uniq_booking?, if: -> { booking_id.present? }

      private

      def correct_reviewer?
        booking.barber_id != user_id
      end

      def uniq_booking?
        errors.add(:base, I18n.t('operation.review.errors.booking_exist')) if review
      end

      def booking
        @booking ||= Booking.find(booking_id)
      end

      def review
        @review ||= Review.find_by(booking_id: booking_id, user_id: user_id)
      end
    end
  end
end
