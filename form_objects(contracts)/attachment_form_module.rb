class Message
  module Contract
    module AttachmentFormModule
      include Reform::Form::Module

      property :attachment, populate_if_empty: ::Attachment do
        property :attachmentable_id
        property :attachmentable_type

        property :file

        validates :file, file_size: { less_than_or_equal_to: Attachment::MAX_FILE_SIZE.megabytes },
                         file_content_type: { allow: Attachment::CONTENT_TYPES }, if: -> { file.present? }
      end
    end
  end
end
