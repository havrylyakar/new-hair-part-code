module Api
  module V1
    class ReviewsController < Api::ApiBaseController
      def create
        run_and_render ::Review::Create, params
      end

      def index
        run_and_render ::Review::Index, params
      end
    end
  end
end
