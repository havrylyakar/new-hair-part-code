module Api
  module V1
    class ServicesController < Api::ApiBaseController
      def create
        run_and_render ::Service::Create, params_normalizer.deserialized_params
      end

      def update
        run_and_render ::Service::CollectionUpdate, params_normalizer.deserialized_params
      end

      def update_relations
        run_and_render ::Service::Update, params_normalizer.deserialized_params.merge(id: params[:id])
      end

      def add_attachments
        run_and_render ::Service::AddAttachments
      end

      def destroy
        run_and_render ::Service::Destroy, params
      end

      def show
        run_and_render ::Service::Show, params
      end

      def index
        run_and_render ::Service::Index, params
      end
    end
  end
end
