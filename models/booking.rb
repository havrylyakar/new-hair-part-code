class Booking < ApplicationRecord
  STATUSES = [CREATED = 'created'.freeze, CANCELED = 'canceled'.freeze,
              PAID = 'paid'.freeze, PROCESSING = 'processing'.freeze,
              FAILED = 'failed'.freeze].freeze

  belongs_to :user
  belongs_to :barber, class_name: 'User', foreign_key: 'barber_id', primary_key: 'id'

  has_many :services, through: :bookings_services
  has_many :bookings_services, dependent: :destroy

  has_many :service_dates

  has_many :prices, dependent: :destroy

  has_one :review

  delegate :full_name, to: :barber, prefix: true, allow_nil: true
  delegate :full_name, to: :user, prefix: true, allow_nil: true

  scope :actual, -> { where.not(status: [PAID, CANCELED]) }
end
