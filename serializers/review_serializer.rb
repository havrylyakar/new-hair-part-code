class ReviewSerializer < BaseSerializer
  attributes :description, :rate, :user_id, :created_at, :booking_id

  belongs_to :user, serializer: UserSerializer
  belongs_to :booking, serializer: BookingSerializer
  has_one :attachment, serializer: AttachmentSerializer
end
