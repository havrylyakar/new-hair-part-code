class BookingSerializer < BaseSerializer
  delegate :amount, to: 'object.decorate'

  Booking.column_names.each { |col| attributes col.to_sym }

  has_many :service_dates, serializer: ServiceDateSerializer
  has_many :services, serializer: ServiceSerializer
  belongs_to :user, serializer: UserSerializer
  belongs_to :barber, serializer: UserSerializer
  has_one :review, serializer: ReviewSerializer
  has_many :prices, serializer: PriceSerializer

  attribute :amount
end
