class NotificationsService
  attr_reader :object, :users

  def initialize(users, object)
    @users = [*users]
    @object = object
  end

  Notification::TYPES.each do |type|
    define_method type.to_s do
      users.each { |user| add_new_notification(user, type) }
    end
  end

  private

  def uniq?(user, type)
    user.notifications.where(object_id: object.id, object_type: type).empty?
  end

  def add_new_notification(user, type)
    return unless uniq?(user, type)
    notification = notification(type)
    user.notifications.create notification
    push_notifications(user, type, notification)
  end

  def push_notifications(user, type, notification)
    ApnService.new.push(user, type, notification)
  end

  def set_title_new_message
    I18n.t('notifications.new_message', user_name: object.user_full_name)
  end

  def set_title_new_booking
    I18n.t('notifications.new_booking', user_name: object.user_full_name)
  end

  def set_title_new_like
    return new_first_like_message if correct_like_count.zero?
    I18n.t('notifications.new_like', user_name: object.user_full_name,
                                     count_likes: correct_like_count)
  end

  def correct_like_count
    @correct_like_count ||= object.activity.decorate.like_count - 1
  end

  def new_first_like_message
    I18n.t('notifications.new_first_like', user_name: object.user_full_name)
  end

  def set_custom_data_new_message
    {
      message: object.text
    }
  end

  def set_custom_data_new_booking; end

  def set_custom_data_new_like; end

  def notification(type)
    {
      object_type: type,
      title: send("set_title_#{type}"),
      object_id: object&.id,
      custom_data: send("set_custom_data_#{type}")
    }
  end
end
